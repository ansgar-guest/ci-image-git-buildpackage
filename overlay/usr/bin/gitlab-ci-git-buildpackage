#!/bin/bash -e

. /usr/share/gitlab-ci-common

# these are all required
! test -z "$CI_PROJECT_DIR"
! test -z "$CI_COMMIT_SHA"
! test -z "$CI_COMMIT_TAG"
! test -z "$CI_COMMIT_REF_NAME"

set -x

# delete any possible remnants from previous runs, except orig tarballs
find $(dirname $CI_PROJECT_DIR) -maxdepth 1 -type f -name '*-[0-9]*.*' -print -delete

$apt_get update

cd $CI_PROJECT_DIR
if dpkg --compare-versions `dpkg -s apt |sed -n 's,^Version: \(.*\),\1,p'` ge 1.1; then
    $apt_get -o APT::Get::Build-Dep-Automatic=yes build-dep $CI_PROJECT_DIR
else
    # this is running on an older version, like jessie-backports
    export BUILD_DEPENDS=`sed 's/\s*|[^,]*,/,/g' debian/control | perl -ne 'next if /^#/; $p=(s/^Build-Depends:\s*/ / or (/^ / and $p)); s/,|\n|\([^)]+\)//mg; print if $p'`
    $apt_get install --no-install-recommends \
             build-essential fakeroot git-buildpackage pristine-tar $BUILD_DEPENDS
fi
dpkg-checkbuilddeps
debversion=$(dpkg-parsechangelog -S Version)
buildpackage_options=
if [[ $debversion = *-* ]]; then
    echo "ensure pristine-tar branch exists and is current"
    git clean -fdx
    git reset --hard
    git fetch --all
    uscan_download="true"
    if git checkout -B pristine-tar origin/pristine-tar; then
        orig=$(pristine-tar list| head -1)
        tar_version=`dpkg -s tar | grep '^Version:'`
        if pristine-tar checkout $orig; then
            uscan_download="false"
            echo "pristine-tar works"
        elif [[ $tar_version == "Version: 1.30"* ]]; then
            echo "WARNING: tar v1.30 breaks pristine-tar when working with commits created by older versions of tar https://bugs.debian.org/901952, downgrading to tar 1.29b-1.1"
            printf 'Package: tar\nPin: version 1.30+dfsg-2\nPin-Priority: -1\n' \
                   > /etc/apt/preferences.d/ban-broken-tar.pref
            $apt_get install --no-install-recommends wget
            wget -q http://deb.debian.org/debian/pool/main/t/tar/tar_1.29b-1.1_amd64.deb
            dpkg -i tar_1.29b-1.1_amd64.deb
            rm -f tar_1.29b-1.1_amd64.deb
        fi
        rm -f $orig
        buildpackage_options=--git-pristine-tar
    fi

    if [ $uscan_download = "true" ]; then
        echo "no pristine-tar branch, trying uscan";
        apt_get_auto_install gnupg2 libwww-perl;
        uscan --verbose --download-current-version --force-download --rename;
    fi
else
    echo "building native package version $debversion"
fi
echo "make $CI_COMMIT_REF_NAME branch current for gbp"
git checkout -B "$CI_COMMIT_REF_NAME" "$CI_COMMIT_SHA"
if [ -z "$CI_COMMIT_TAG" ]; then
    gbp buildpackage -uc -us $buildpackage_options --git-debian-branch="$CI_COMMIT_REF_NAME"
else
    gbp buildpackage -uc -us $buildpackage_options
fi
